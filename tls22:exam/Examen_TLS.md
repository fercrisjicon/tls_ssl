# Examen Practica Cristian Condolo

**CA** = **VeritatCondolo** - **1000 dias**

- crear la clave priv del CA

> openssl genrsa -out cakey.pem 1048

**Verifica la clave priv**:
> openssl rsa --noout --text -in cakey.pem
> file cakey.pem

- generar el certificado de la CA

> openssl req -new -x509 -days 1000 -key cakey.pem -out ca_veritatcondolo_cert.pem

```
-----
Country Name (2 letter code) [AU]:__CA__
State or Province Name (full name) [Some-State]:__CA__
Locality Name (eg, city) []:__BCN__
Organization Name (eg, company) [Internet Widgits Pty Ltd]:__Veritat Condolo__
Organizational Unit Name (eg, section) []:__veritatcondolo__
Common Name (e.g. server FQDN or YOUR name) []:__condolo.veritat.org__
Email Address []:__condolo@veritat.org__
```

**Verifica certificado CA**:
> openssl x509 --noout --text -in ca_veritatcondolo_cert.pem
> file ca_veritatcondolo_cert.pem

- generar extencion para el fichero conf de openssl (extserver.cnf)

```cnf
[ practica_req ]

# Extensions to add to a certificate request
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]
DNS.0 = aaron.edt.org
DNS.1 = mysecureldapserver.org
DNS.2 = ldap
IP.1 = 172.18.0.2
IP.2 = 127.0.0.1
```

esta extencion la añadiremos al fondo del fichero `/etc/ssl/openssl.cnf`

- generar request que tambien creare un clave priv para el servido (serverkey_ldap.pem) y la request (serverrequest_ldap.pem) a partir del fichero conf que acabamos de modificar para LDAP
**passphrase: veritatcondolo**

> openssl req -newkey rsa:2048 -sha256 -keyout serverkey_ldap.pem -out serverrequest_ldap.pem -config /etc/ssl/openssl.cnf

```
-----
Country Name (2 letter code) [AU]:__CA__
State or Province Name (full name) [Some-State]:__CA__
Locality Name (eg, city) []:__BCN__
Organization Name (eg, company) [Internet Widgits Pty Ltd]:__EDT__
Organizational Unit Name (eg, section) []:__LDAP__
Common Name (e.g. server FQDN or YOUR name) []:__ldap.edt.org__
Email Address []:__ldap@edt.org__

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:__jupiter__
An optional company name []:__ldap__
```

**Verifica Server request LDAP**:
> openssl req -noout -text -in serverrequest_ldap.pem

- firmar el certificado y general con la clave priv de CA

> openssl x509 -CAkey cakey.pem -CA ca_veritatcondolo_cert.pem -req -in serverrequest_ldap.pem -days 3650 -CAcreateserial -out servercert_ldap.pem -extensions 'practica_req' -extfile extserver.cnf

```

Signature ok
subject=C = CA, ST = CA, L = BCN, O = EDT, OU = LDAP, CN = ldap.edt.org, emailAddress = ldap@edt.org
Getting CA Private Key
```

**Verifica certificado Server LDAP**:
> openssl x509 -noout -text -in servercert_ldap.pem

- crear un nuevo dir `/etc/ldap/certs` y colocar el certificado CA y su clave priv

- modificar nuestro `ldap.conf`:

> sudo vim /etc/ldap/ldap.conf

```conf
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE dc=example,dc=com
#URI ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT 12
#TIMELIMIT 15
#DEREF  never

# TLS certificates (needed for GnuTLS)
#TLS_CACERT /etc/ssl/certs/ca-certificates.crt
TLS_CACERT /etc/ssl/certs/ca_veritatcondolo_cert.pem

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```

- **tener con antelacion los ficheros de ldap:grup** editar las siguientes ficheros:
  - Dockerfile (instalar el openssl)

    ```dockerfile
    FROM debian:11
    LABEL version="1.0"
    LABEL author="@fercrisjicon ASIX-M11"
    LABEL subject="TLS Ldap Server cn dataDB"
    RUN apt-get update
    ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get -y install procps iproute2 tree nmap nano vim systemd slapd ldap-utils openssl
    RUN mkdir /opt/docker
    COPY . /opt/docker/
    RUN chmod +x /opt/docker/startup.sh
    WORKDIR /opt/docker
    CMD /opt/docker/startup.sh
    EXPOSE 389 636
    ```

  - startup.sh

    ```sh
    #! /bin/bash
    # tls22:ldaps
    # @fercrisjicon ASIX M11-SAD Curs 2021-2022
    mkdir /etc/ldap/certs

    # Ficheros de configuracion
    cp /opt/docker/ca_veritatcondolo_cert.pem /etc/ldap/certs/. # para probar que es el mismo
    cp /opt/docker/ca_veritatcondolo_cert.pem /etc/ssl/certs/. # certificado de CA
    cp /opt/docker/servercert_ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
    cp /opt/docker/serverkey_ldap.pem /etc/ldap/certs/. # clave pub/priv del Server

    # Limpieza
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*

    cp /opt/docker/slapd.conf /etc/ldap/slapd.conf

    # Database
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slaptest -u -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
    chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

    # Ficheros de configuracion
    cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

    # Encender servers, en detach
    /usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
    ```

  - ldap.conf

    ```conf
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    #BASE dc=example,dc=com
    #URI ldap://ldap.example.com ldap://ldap-provider.example.com:666

    #SIZELIMIT 12
    #TIMELIMIT 15
    #DEREF  never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT /etc/ssl/certs/ca_veritatcondolo_cert.pem

    BASE dc=edt,dc=org
    URI ldap://ldap.edt.org
    ```

  - slapd.conf (añadimos al final o en medio, como prefieras)

    ```conf
    # Certificados-Practica ================================
    TLSCACertificateFile        /etc/ldap/certs/ca_veritatcondolo_cert.pem
    TLSCertificateFile  /etc/ldap/certs/servercert_ldap.pem
    TLSCertificateKeyFile       /etc/ldap/certs/serverkey_ldap.pem
    TLSVerifyClient       never
    #=======================================================
    ```
