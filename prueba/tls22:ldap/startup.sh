#! /bin/bash
# tls22:ldaps
# @fercrisjicon ASIX M11-SAD Curs 2021-2022
#mkdir /etc/ldap/certs

# Ficheros de configuracion
#cp /opt/docker/ca_johnwick_cert.pem /etc/ldap/certs/. # para probar que es el mismo
#cp /opt/docker/ca_johnwick_cert.pem /etc/ssl/certs/. # certificado de CA
#cp /opt/docker/servercert_ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
#cp /opt/docker/serverkey_ldap.pem /etc/ldap/certs/. # clave pub/priv del Server

# Limpieza
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*

cp /opt/docker/slapd.conf /etc/ldap/slapd.conf

# Database
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slaptest -u -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Ficheros de configuracion
cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

# Encender servers, en detach
/usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
