# Teoria
## Ejemplos
### CLAVES PRIVADAS RSA
- veamos cuantas opciones tiene ```openssl```, tabulando dos veces.
> openssl
```
aes-128-cbc       ciphers           engine            rc2-ofb
aes-128-ecb       crl               errstr            rc4
aes-192-cbc       crl2pkcs7         gendh             rc4-40
aes-192-ecb       des               gendsa            req
aes-256-cbc       des3              genpkey           rmd160
aes-256-ecb       des-cbc           genrsa            rsa
asn1parse         des-cfb           md2               rsautl
base64            des-ecb           md4               s_client
bf                des-ede           md5               sess_id
bf-cbc            des-ede3          nseq              sha
bf-cfb            des-ede3-cbc      ocsp              sha1
bf-ecb            des-ede3-cfb      passwd            sha224
bf-ofb            des-ede3-ofb      pkcs12            sha256
ca                des-ede-cbc       pkcs7             sha384
camellia-128-cbc  des-ede-cfb       pkcs8             sha512
camellia-128-ecb  des-ede-ofb       pkey              smime
camellia-192-cbc  des-ofb           pkeyparam         speed
camellia-192-ecb  desx              pkeyutl           spkac
camellia-256-cbc  dgst              prime             s_server
camellia-256-ecb  dh                rand              s_time
cast              dhparam           rc2               verify
cast5-cbc         dsa               rc2-40-cbc        version
cast5-cfb         dsaparam          rc2-64-cbc        x509
cast5-ecb         ec                rc2-cbc           
cast5-ofb         ecparam           rc2-cfb           
cast-cbc          enc               rc2-ecb           
```
- creamos una clave privada en formato des3
> openssl genrsa -des3 --out cakey.pem 2048

--out : te la vomita como cakey.pem
2048: num de bits
```
Generating RSA private key, 2048 bit long modulus (2 primes)
...+++++
....+++++
e is 65537 (0x010001)
Enter pass phrase for cakey.pem: cakey
Verifying - Enter pass phrase for cakey.pem: cakey
```
- vemos que tipo de fichero es la clave
> file cakey.pem
```
cakey.pem: PEM RSA private key
```

es la tranformacion de la clve privada de un cifrado DES3

- que nos muestre
> openssl rsa --noout --text -in cakey.pem

--noout: que no vuelva a vomitar todo el listado, la clave
```
Enter pass phrase for cakey.pem:
RSA Private-Key: (2048 bit, 2 primes)
modulus:
    00:bc:d2:b2:c6:97:37:1a:f1:8d:c4:14:33:a6:41:
    ea:34:3f:9c:b8:01:bc:ed:29:1d:28:a8:04:b4:29:
    04:a6:c2:bc:f0:dc:32:30:0d:c4:e2:01:43:89:80:
    b9:35:78:c7:91:ca:d0:86:cd:b1:c3:91:e4:e2:a9:
    26:36:b7:4c:d5:6c:47:8b:d3:27:ba:76:b6:88:d9:
    0e:e3:45:9f:c6:73:72:f5:41:1b:25:f3:41:35:df:
    86:df:c8:c8:1d:86:42:3b:93:e8:bc:bb:8a:71:98:
    3b:41:c0:aa:8b:c6:b6:21:df:f3:40:d6:b6:82:16:
    c5:bf:d2:da:ce:b6:4a:70:a1:39:4b:d8:1c:7e:09:
    97:ba:de:f1:cb:df:43:ea:8a:53:96:7d:13:c0:61:
...
```
- creamos otra clave privada
> openssl genrsa -out serverkey.pem 4096

-nodes: no encriptar. **pero no hace falta, ya viene por defecto**
```
Generating RSA private key, 4096 bit long modulus (2 primes)
..................................................++++
.........................................++++
e is 65537 (0x010001)
```
el listado es mas grande que la anterior.

> openssl rsa -noout --text -in serverkey.pem
```
RSA Private-Key: (4096 bit, 2 primes)
modulus:
    00:e5:55:8c:5e:cf:39:15:88:5a:a9:07:24:23:3b:
    4e:05:36:d2:56:f3:be:64:fc:60:a7:f2:6e:cb:35:
    a3:3c:dd:8c:b0:40:25:e1:4e:74:d8:87:e0:73:04:
    61:f8:fe:7c:01:4f:f1:5e:43:c5:64:03:08:d5:c6:
    3c:af:8a:ed:08:cd:07:9a:a2:fb:be:9f:ef:6c:b3:
    85:93:79:df:db:66:34:cc:0f:c8:f1:d6:dc:d7:6b:
    9d:80:7f:51:3b:67:5f:bf:e9:e2:19:b7:53:fd:c2:
    2f:41:c6:7f:41:78:2d:33:b9:c0:08:39:90:90:69:
    5f:45:92:48:3c:61:b0:6d:cd:92:2d:28:d4:90:f9:
    45:cc:7a:51:bc:9a:4f:eb:75:ad:8c:86:ec:b5:a1:
...
```

> openssl rsa -des3 -in serverkey.pem -out serverkey2.pem
```
writing RSA key
Enter PEM pass phrase: serverkey
Verifying - Enter PEM pass phrase: serverkey
```

> openssl rsa -noout --text -in serverkey2.pem
```
Enter pass phrase for serverkey2.pem: serverkey
RSA Private-Key: (4096 bit, 2 primes)
modulus:
    00:e5:55:8c:5e:cf:39:15:88:5a:a9:07:24:23:3b:
    4e:05:36:d2:56:f3:be:64:fc:60:a7:f2:6e:cb:35:
    a3:3c:dd:8c:b0:40:25:e1:4e:74:d8:87:e0:73:04:
    61:f8:fe:7c:01:4f:f1:5e:43:c5:64:03:08:d5:c6:
    3c:af:8a:ed:08:cd:07:9a:a2:fb:be:9f:ef:6c:b3:
    85:93:79:df:db:66:34:cc:0f:c8:f1:d6:dc:d7:6b:
    9d:80:7f:51:3b:67:5f:bf:e9:e2:19:b7:53:fd:c2:
    2f:41:c6:7f:41:78:2d:33:b9:c0:08:39:90:90:69:
    5f:45:92:48:3c:61:b0:6d:cd:92:2d:28:d4:90:f9:
    45:cc:7a:51:bc:9a:4f:eb:75:ad:8c:86:ec:b5:a1:
    a7:84:31:90:7b:c5:50:16:40:ec:b6:d3:5b:63:10:
...
```
> openssl rsa -in serverkey.pem -pubout -out serverpub.pem
```
writing RSA key
```

> openssl rsa -in serverkey.pem --outform DER -out serverkey.der
```
writing RSA key
```

```
isx41016667@i10:/var/tmp/m11/tls_ssl$ cat serverkey.pem | tail -n +2 | head -n -1 > /tmp/f1
isx41016667@i10:/var/tmp/m11/tls_ssl$ base64 --decode /tmp/f1 > /tmp/key.der
isx41016667@i10:/var/tmp/m11/tls_ssl$ file /tmp/key.der
/tmp/key.der: DER Encoded Key Pair, 4096 bits
isx41016667@i10:/var/tmp/m11/tls_ssl$ diff serverkey.der /tmp/key.der
```

borramos todos.

### REQUEST
> openssl req -new -x509 -nodes -out servercert.pem -keyout serverkey.

-new:
-nodes:
-keyout: 
```
Generating a RSA private key
..................................................+++++
.....+++++
writing new private key to 'serverkey.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
```
anunciado
```
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:catalunya
Locality Name (eg, city) []:barcelona
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:m11
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:admin@edt.org
```

> ls
```
servercert.pem  serverkey.pem
```

> openssl x509 --noout --text -in servercert.pem
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            2c:a7:c2:dd:7d:4c:1b:41:3a:e7:bd:66:e1:2f:b8:5d:28:f9:c3:17
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = catalunya, L = barcelona, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
        Validity
            Not Before: Mar 31 11:15:58 2022 GMT
            Not After : Apr 30 11:15:58 2022 GMT
        Subject: C = ca, ST = catalunya, L = barcelona, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:ac:57:d1:4f:f2:d8:49:e2:18:00:96:78:62:bf:
                    ea:3f:cb:79:c5:ad:c8:cb:f9:7e:87:c3:98:f9:d9:
                    d1:ff:9b:c1:14:ef:43:f0:18:3d:e0:4a:b3:7a:d4:
                    ec:94:b3:5c:a8:a3:8a:da:f8:26:ef:91:04:b7:64:
                    1c:a7:65:f1:ac:82:02:ea:53:30:3a:71:95:83:8e:
                    49:e3:df:40:ae:a0:c9:19:ce:26:1e:52:25:ff:3a:
                    ce:fc:21:54:ad:21:e4:55:ba:aa:f7:0f:5c:30:dd:
                    a4:de:8b:2b:ac:e9:44:d7:f9:3a:46:4e:e3:2f:44:
                    28:8f:37:26:68:4a:23:c8:e2:b3:4e:05:72:2f:78:
...
```

```
...
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                2C:EF:7A:2B:78:ED:B4:EA:97:E4:3C:1F:CC:5C:82:A2:AE:9A:57:F0
            X509v3 Authority Key Identifier: 
                keyid:2C:EF:7A:2B:78:ED:B4:EA:97:E4:3C:1F:CC:5C:82:A2:AE:9A:57:F0

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         3b:b8:fa:cf:bc:6f:a1:de:53:6c:85:c6:6e:a4:19:48:d4:0a:
         4c:1c:76:96:19:66:68:cf:50:8a:22:21:5c:ea:4a:17:33:60:
         5a:20:bc:12:fc:8e:ff:62:2a:57:11:bc:07:9d:8a:47:d0:5e:
         e7:1a:e5:e7:9b:cb:d7:c4:5c:a7:63:d4:1f:a5:35:a3:ab:44:
         90:4e:9f:64:ff:f0:0a:a7:c9:3b:9c:2e:9e:02:48:84:1f:63:
         05:19:0b:f2:af:ed:d6:3d:6a:4e:49:40:1b:a8:b3:e2:de:af:
         3d:fb:27:32:f7:ff:fa:af:77:e5:ab:93:82:ae:4e:27:74:2d:
         b1:9c:28:9b:9f:91:be:3a:b2:ad:c3:70:e1:bc:de:f5:d5:6f:
         04:d7:56:94:20:d6:93:d5:b1:34:b1:74:a4:55:5e:52:6b:44:
         4c:14:91:32:19:17:28:65:7d:8c:33:13:75:86:f6:b7:6d:4d:
         07:3f:97:7d:e0:9e:6a:58:0a:5c:2d:18:72:ae:e0:99:d0:c4:
         fd:0f:f4:cd:cb:8d:e4:e6:da:ab:d8:c3:ff:81:1b:56:3a:ab:
         51:53:f7:e9:0a:e7:fc:60:f6:82:4e:3e:fc:0b:a0:cc:b0:ba:
         f6:2f:b8:df:09:21:21:d3:e8:9e:1b:b6:79:da:3b:1e:c4:5a:
         ce:cf:5b:02
```
esto la firma del **issuer**, la autoridad de certificacion.

> openssl x509 --noout --issuer -subject -purpose -dates -in servercert.pem 

-purpose: saber para que proposito sirve el certificado.
```
issuer=C = ca, ST = catalunya, L = barcelona, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
subject=C = ca, ST = catalunya, L = barcelona, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
Certificate purposes:
SSL client : Yes
SSL client CA : Yes
SSL server : Yes
SSL server CA : Yes
Netscape SSL server : Yes
Netscape SSL server CA : Yes
S/MIME signing : Yes
S/MIME signing CA : Yes
S/MIME encryption : Yes
S/MIME encryption CA : Yes
CRL signing : Yes
CRL signing CA : Yes
Any Purpose : Yes
Any Purpose CA : Yes
OCSP helper : Yes
OCSP helper CA : Yes
Time Stamp signing : No
Time Stamp signing CA : Yes
notBefore=Mar 31 11:15:58 2022 GMT
notAfter=Apr 30 11:15:58 2022 GMT
```
> openssl req -new -x509 -out servercert.pem -newkey rsa:2048 -keyout serverkey.pem
```
Generating a RSA private key
..................................+++++
..............................................................................................................................+++++
writing new private key to 'serverkey.pem'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:cat
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:catalunya
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:catalunya
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:m11
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:admin@edt.org
```

> openssl x509 --noout --issuer -subject -dates -in servercert.pem
```
issuer=C = ca, ST = catalunya, L = bcn, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
subject=C = ca, ST = catalunya, L = bcn, O = edt, OU = m11, CN = web.edt.org, emailAddress = admin@edt.org
notBefore=Mar 31 11:28:17 2022 GMT
notAfter=Apr 30 11:28:17 2022 GMT
```

borramos todo de nuevo

> openssl genrsa -out cakey.pem
```
Generating RSA private key, 2048 bit long modulus (2 primes)
.......................................................................................................................................................................................................................+++++
..........................................+++++
e is 65537 (0x010001)
```

> openssl req -new -x509 -days 365 -key cakey.pem -out cacert.pem
```
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:VeritatAbsoluta             
Organizational Unit Name (eg, section) []:certificats
Common Name (e.g. server FQDN or YOUR name) []:veritat
Email Address []:veritat@veritat.cat
```

> openssl x509 --noout -issuer -subject -dates -in cacert.pem 
```
issuer=C = ca, ST = cat, L = bcn, O = VeritatAbsoluta, OU = certificats, CN = veritat, emailAddress = veritat@veritat.cat
subject=C = ca, ST = cat, L = bcn, O = VeritatAbsoluta, OU = certificats, CN = veritat, emailAddress = veritat@veritat.cat
notBefore=Mar 31 11:34:11 2022 GMT
notAfter=Mar 31 11:34:11 2023 GMT
```

estamos en otro usuario
> openssl genrsa -out serverkey.pem

- fabricar una perticion de certificado
> openssl req -new -keyserverkey.pem -out serverrequest.pem
```
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:EscoladelTreball
Organizational Unit Name (eg, section) []:Informatica
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:admin@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:edt
```

>openssl req -noout -text -in serverrequest.pem | less

>openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverrequest.pem -CAcreateserial -days 90 -out servercert.pem
```
serverrequest.pem -CAcreateserial -days 90 -out servercert.pem
Signature ok
subject=C = ca, ST = cat, L = bcn, O = EscoladelTreball, OU = Informatica, CN = web.edt.org, emailAddress = admin@edt.org
Getting CA Private Key
```

> openssl x509 -noout --text -in servercert.pem 
```
Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            12:87:48:c2:f8:ec:76:da:b5:11:5e:6e:e0:7d:0d:ad:fb:60:04:d1
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = cat, L = bcn, O = VeritatAbsoluta, OU = certificats, CN = veritat, emailAddress = veritat@veritat.cat
        Validity
            Not Before: Mar 31 11:43:59 2022 GMT
            Not After : Jun 29 11:43:59 2022 GMT
        Subject: C = ca, ST = cat, L = bcn, O = EscoladelTreball, OU = Informatica, CN = web.edt.org, emailAddress = admin@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:bf:cb:96:e8:00:5a:9b:e0:c6:88:d6:0e:76:02:
                    43:cb:cf:6c:fd:dc:d8:66:e4:87:8f:d9:84:60:f3:
                    82:6f:da:96:60:ba:63:4c:77:17:b8:87:bd:68:94:
                    14:a0:0f:30:a8:11:57:8c:84:33:da:df:62:5d:20:
                    c0:17:e6:1a:77:b1:00:cb:25:6d:85:e9:9e:12:b7:
                    a0:11:a1:8c:91:2e:c3:61:ba:27:96:24:1c:d2:ce:
                    e8:f9:f0:77:69:03:b9:a9:07:a0:86:3c:99:2a:90:
                    5d:c4:82:fc:55:9e:19:23:aa:32:37:0e:cf:00:d2:
                    13:2a:52:9a:f7:ac:ce:74:bd:92:e1:25:82:5a:11:
                    ad:70:60:ac:71:14:dc:77:9a:9f:e8:0c:91:6b:19:
                    43:dd:e5:5a:3d:f5:82:e6:a6:c3:4b:b4:f1:0c:16:
                    dd:9a:f1:5e:e4:70:c5:b8:8d:b9:ea:ab:19:01:b0:
                    cf:aa:e3:9d:a7:b1:3b:03:23:ff:fe:e7:51:6e:c8:
                    45:a7:38:ef:04:a7:7e:8a:1d:f9:3c:9d:99:08:61:
                    28:d7:08:41:35:6c:4f:39:66:ce:a5:50:e0:db:60:
                    7a:a9:c8:7f:99:ba:3b:35:c4:c8:17:85:60:5e:3b:
                    24:e4:fb:51:5b:3a:90:7e:7b:50:ad:24:eb:4b:1b:
                    70:53
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         b1:0e:7d:b7:d3:d7:82:97:07:53:f7:c5:59:60:66:dc:69:07:
         23:45:5e:c0:6d:14:f3:b6:49:59:91:26:8d:89:06:e3:cf:dc:
         27:a7:21:dd:01:94:9f:72:9e:cc:c5:1b:1f:e6:ea:71:5d:95:
         aa:59:a0:c0:12:30:7f:86:4f:4f:00:e0:96:a7:75:69:2d:35:
         05:95:ce:0e:6d:5f:ce:bd:1d:28:b2:9a:43:e3:8a:e2:0f:cc:
         06:53:c5:c8:c0:a8:c3:25:b5:d5:7b:4c:dc:45:9d:1d:1b:63:
         dc:10:cf:2a:14:23:6e:21:89:be:a2:f4:ea:10:49:44:19:95:
         6f:02:ff:4f:ee:72:5b:14:5e:81:5e:6c:ad:bb:5c:b7:e8:5d:
         df:c1:7d:4e:e5:3b:a0:6c:6c:fc:d0:88:52:65:db:69:5b:1d:
         87:cb:15:67:bd:19:ed:02:d0:62:ba:0f:b8:1a:75:8f:8e:05:
         34:a7:50:f2:e9:8c:84:0d:5f:18:c5:f9:55:a1:8c:58:de:e5:
         8a:ff:30:31:fa:6b:fa:93:94:3c:62:2d:d5:44:7a:0e:76:26:
         28:ec:fd:42:79:25:ca:ae:06:76:0d:4b:63:23:21:9d:a3:c7:
         9b:e3:34:d9:d6:57:48:0e:a8:18:9e:52:95:15:d9:7b:46:4a:
         43:3b:9c:a5
```

> openssl x509 -noout -purpose --text -in servercert.pem 
```
Certificate purposes:
SSL client : Yes
SSL client CA : No
SSL server : Yes
SSL server CA : No
Netscape SSL server : Yes
Netscape SSL server CA : No
S/MIME signing : Yes
S/MIME signing CA : No
S/MIME encryption : Yes
S/MIME encryption CA : No
CRL signing : Yes
CRL signing CA : No
Any Purpose : Yes
Any Purpose CA : Yes
OCSP helper : Yes
OCSP helper CA : No
Time Stamp signing : No
Time Stamp signing CA : No
Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            12:87:48:c2:f8:ec:76:da:b5:11:5e:6e:e0:7d:0d:ad:fb:60:04:d1
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = cat, L = bcn, O = VeritatAbsoluta, OU = certificats, CN = veritat, emailAddress = veritat@veritat.cat
        Validity
            Not Before: Mar 31 11:43:59 2022 GMT
            Not After : Jun 29 11:43:59 2022 GMT
        Subject: C = ca, ST = cat, L = bcn, O = EscoladelTreball, OU = Informatica, CN = web.edt.org, emailAddress = admin@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:bf:cb:96:e8:00:5a:9b:e0:c6:88:d6:0e:76:02:
                    43:cb:cf:6c:fd:dc:d8:66:e4:87:8f:d9:84:60:f3:
                    82:6f:da:96:60:ba:63:4c:77:17:b8:87:bd:68:94:
                    14:a0:0f:30:a8:11:57:8c:84:33:da:df:62:5d:20:
                    c0:17:e6:1a:77:b1:00:cb:25:6d:85:e9:9e:12:b7:
                    a0:11:a1:8c:91:2e:c3:61:ba:27:96:24:1c:d2:ce:
                    e8:f9:f0:77:69:03:b9:a9:07:a0:86:3c:99:2a:90:
                    5d:c4:82:fc:55:9e:19:23:aa:32:37:0e:cf:00:d2:
                    13:2a:52:9a:f7:ac:ce:74:bd:92:e1:25:82:5a:11:
                    ad:70:60:ac:71:14:dc:77:9a:9f:e8:0c:91:6b:19:
                    43:dd:e5:5a:3d:f5:82:e6:a6:c3:4b:b4:f1:0c:16:
                    dd:9a:f1:5e:e4:70:c5:b8:8d:b9:ea:ab:19:01:b0:
                    cf:aa:e3:9d:a7:b1:3b:03:23:ff:fe:e7:51:6e:c8:
                    45:a7:38:ef:04:a7:7e:8a:1d:f9:3c:9d:99:08:61:
                    28:d7:08:41:35:6c:4f:39:66:ce:a5:50:e0:db:60:
                    7a:a9:c8:7f:99:ba:3b:35:c4:c8:17:85:60:5e:3b:
                    24:e4:fb:51:5b:3a:90:7e:7b:50:ad:24:eb:4b:1b:
                    70:53
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         b1:0e:7d:b7:d3:d7:82:97:07:53:f7:c5:59:60:66:dc:69:07:
         23:45:5e:c0:6d:14:f3:b6:49:59:91:26:8d:89:06:e3:cf:dc:
         27:a7:21:dd:01:94:9f:72:9e:cc:c5:1b:1f:e6:ea:71:5d:95:
         aa:59:a0:c0:12:30:7f:86:4f:4f:00:e0:96:a7:75:69:2d:35:
         05:95:ce:0e:6d:5f:ce:bd:1d:28:b2:9a:43:e3:8a:e2:0f:cc:
         06:53:c5:c8:c0:a8:c3:25:b5:d5:7b:4c:dc:45:9d:1d:1b:63:
         dc:10:cf:2a:14:23:6e:21:89:be:a2:f4:ea:10:49:44:19:95:
         6f:02:ff:4f:ee:72:5b:14:5e:81:5e:6c:ad:bb:5c:b7:e8:5d:
         df:c1:7d:4e:e5:3b:a0:6c:6c:fc:d0:88:52:65:db:69:5b:1d:
         87:cb:15:67:bd:19:ed:02:d0:62:ba:0f:b8:1a:75:8f:8e:05:
         34:a7:50:f2:e9:8c:84:0d:5f:18:c5:f9:55:a1:8c:58:de:e5:
         8a:ff:30:31:fa:6b:fa:93:94:3c:62:2d:d5:44:7a:0e:76:26:
         28:ec:fd:42:79:25:ca:ae:06:76:0d:4b:63:23:21:9d:a3:c7:
         9b:e3:34:d9:d6:57:48:0e:a8:18:9e:52:95:15:d9:7b:46:4a:
         43:3b:9c:a5
```

- creamos un mini fichero de configuracion
> vim ca.conf
```conf
basicConstraints = critical,CA:FALSE
extendedKeyUsage = serverAuth,emailProtection
```

> openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverrequest.pem -CAcreateserial -days 360 -extfile ca.conf -out servercert.pem
```
Signature ok
subject=C = ca, ST = cat, L = bcn, O = EscoladelTreball, OU = Informatica, CN = web.edt.org, emailAddress = admin@edt.org
Getting CA Private Key
```

> openssl x509 -noout --text -in servercert.pem
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            12:87:48:c2:f8:ec:76:da:b5:11:5e:6e:e0:7d:0d:ad:fb:60:04:d2
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = cat, L = bcn, O = VeritatAbsoluta, OU = certificats, CN = veritat, emailAddress = veritat@veritat.cat
        Validity
            Not Before: Mar 31 11:54:08 2022 GMT
            Not After : Mar 26 11:54:08 2023 GMT
        Subject: C = ca, ST = cat, L = bcn, O = EscoladelTreball, OU = Informatica, CN = web.edt.org, emailAddress = admin@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:bf:cb:96:e8:00:5a:9b:e0:c6:88:d6:0e:76:02:
                    43:cb:cf:6c:fd:dc:d8:66:e4:87:8f:d9:84:60:f3:
                    82:6f:da:96:60:ba:63:4c:77:17:b8:87:bd:68:94:
                    14:a0:0f:30:a8:11:57:8c:84:33:da:df:62:5d:20:
                    c0:17:e6:1a:77:b1:00:cb:25:6d:85:e9:9e:12:b7:
                    a0:11:a1:8c:91:2e:c3:61:ba:27:96:24:1c:d2:ce:
                    e8:f9:f0:77:69:03:b9:a9:07:a0:86:3c:99:2a:90:
                    5d:c4:82:fc:55:9e:19:23:aa:32:37:0e:cf:00:d2:
                    13:2a:52:9a:f7:ac:ce:74:bd:92:e1:25:82:5a:11:
                    ad:70:60:ac:71:14:dc:77:9a:9f:e8:0c:91:6b:19:
                    43:dd:e5:5a:3d:f5:82:e6:a6:c3:4b:b4:f1:0c:16:
                    dd:9a:f1:5e:e4:70:c5:b8:8d:b9:ea:ab:19:01:b0:
                    cf:aa:e3:9d:a7:b1:3b:03:23:ff:fe:e7:51:6e:c8:
                    45:a7:38:ef:04:a7:7e:8a:1d:f9:3c:9d:99:08:61:
                    28:d7:08:41:35:6c:4f:39:66:ce:a5:50:e0:db:60:
                    7a:a9:c8:7f:99:ba:3b:35:c4:c8:17:85:60:5e:3b:
                    24:e4:fb:51:5b:3a:90:7e:7b:50:ad:24:eb:4b:1b:
                    70:53
                Exponent: 65537 (0x10001)
```
aparencen los extensions, que antes no aparecieron.
```
        X509v3 extensions:
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication, E-mail Protection
    Signature Algorithm: sha256WithRSAEncryption
         c3:23:39:01:d8:6b:0f:c7:b7:98:b7:5e:e6:f0:8d:59:03:8f:
         7d:9c:5d:5b:b1:3a:c9:04:18:44:c9:cb:19:1e:90:5c:b2:8d:
         20:30:5e:84:1a:cb:93:bf:f1:ec:5d:20:5a:46:ee:b4:d3:4f:
         5a:64:36:5d:5d:8b:c7:d1:a3:c0:a8:5e:49:46:11:d3:a5:e3:
         4d:74:a4:40:02:2b:25:4f:e2:65:f5:d3:c5:a2:76:07:34:6f:
         05:c2:74:9b:df:ee:04:c9:5d:72:bc:31:78:47:8c:0d:8c:8c:
         30:26:75:2b:59:cd:14:76:ec:0c:6a:1f:89:36:10:5a:01:7f:
         bf:f0:0a:2e:82:c5:0c:f9:a3:a7:2f:18:64:d8:93:16:a7:e0:
         ca:44:0d:27:82:03:db:a1:9a:d3:6f:e6:12:f3:34:58:cd:23:
         31:8f:fa:67:19:b5:31:9c:35:b8:92:a9:40:e8:dc:83:21:ba:
         b0:c1:5d:1f:ed:f7:e3:bc:ee:19:72:f7:80:47:16:4b:3d:ec:
         df:ac:3d:cd:b9:d9:e1:76:8f:6c:02:0c:1c:d0:e7:22:af:81:
         fa:aa:8e:b0:06:d8:3d:a8:f3:69:e3:96:78:cc:c5:3a:56:36:
         fd:53:ac:22:ef:06:44:a3:bd:bc:ba:ac:e1:ab:be:c6:1c:09:
         72:11:1d:e7
```

Los CA necesitan un **fichero de configuracion**.
Las **clave priv/pub** pueden estar configurado **dentro del fichero**, no seria necesario añadirlo al comando.

##### Practica
- encender un docker del profesor
> docker run --rm --name web.edt.org -h web.edt.org --net 2hisix -d edtasixm11/tls18:https

- entrar dentro de la maquina
> docker exec -it web.edt.org /bin/bash

- comprobamos que funciona
> ps ax
> nmap localhost

> httpd -S
> vim /etc/httpd/conf.d/webs.conf
> vim /etc/httpd/conf/httpd.conf

dentro del container
> cd /var/www/certs
> ls

- instalar openssl
> yum install openssl

> openssl x509 -noout -text -in servercert.auto1.pem
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            97:c0:ab:6e:3e:42:36:66
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = www.auto1.cat, emailAddress = auto1@edt.org
        Validity
            Not Before: Mar 22 11:25:50 2019 GMT
            Not After : Apr 21 11:25:50 2019 GMT
        Subject: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = www.auto1.cat, emailAddress = auto1@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:d1:28:3a:be:2c:02:3f:fe:a7:3b:5b:6f:42:28:
                    52:79:1e:b7:c8:4a:12:5d:87:53:97:09:bb:8d:7e:
                    2e:85:be:c0:cd:66:73:e6:5f:38:a3:fd:2a:df:43:
                    3f:da:74:c9:2e:91:a7:60:24:a3:76:aa:b4:eb:e0:
                    c5:b1:58:3c:73:19:9b:3f:2f:07:f9:1c:65:95:8c:
                    a6:e7:9c:71:b1:aa:89:e4:7b:f8:7c:98:2c:f8:c8:
                    21:b9:9d:53:da:5a:fc:e0:4a:c7:cb:33:2a:02:19:
                    bc:42:f1:6c:89:97:65:4a:f9:68:89:7d:98:6c:83:
                    44:ab:5b:bd:8b:c2:b9:a7:f9:46:b7:9e:1f:a7:d1:
                    84:f9:30:c0:29:ae:14:50:58:b7:96:47:75:f4:ab:
                    01:9d:40:c0:0f:ac:01:4b:42:8f:a4:3d:8a:51:42:
                    60:3c:eb:87:8f:ae:2b:4d:de:69:c6:b1:9d:60:10:
                    89:76:57:69:3d:95:3d:6b:12:ac:21:40:6a:70:f5:
                    28:e4:40:40:b4:fa:c2:1a:53:64:d3:6e:ea:5b:6f:
                    3c:c5:ff:c8:2d:fe:84:50:2e:2a:ec:01:38:a2:6b:
                    9b:89:d4:23:1f:19:3c:74:3f:59:a4:2f:e5:47:4f:
                    e5:c6:dd:4c:70:8d:f3:2f:98:ee:43:ae:84:ff:aa:
                    5d:03
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                B1:11:E8:33:FA:AB:EF:BD:F8:95:5B:F3:52:81:4D:AA:24:6A:94:39
            X509v3 Authority Key Identifier: 
                keyid:B1:11:E8:33:FA:AB:EF:BD:F8:95:5B:F3:52:81:4D:AA:24:6A:94:39

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         06:37:08:b5:d4:5f:9f:e6:c8:48:5d:0e:02:c0:db:43:69:1c:
         a2:bb:00:eb:f6:e2:42:d0:53:04:05:a5:9d:9d:13:c3:d2:86:
         54:d0:ce:2b:76:11:97:e5:8f:73:71:53:38:7f:85:bf:22:c2:
         22:34:2f:d4:46:64:41:e9:ec:d4:e0:60:2c:b1:3b:9f:ba:f1:
         3c:54:8c:14:9e:08:04:69:d4:6f:9d:ed:fd:84:c1:d5:7c:93:
         15:e6:7c:0a:e5:2f:fa:74:2b:4c:5a:ab:41:7c:0b:db:c8:70:
         49:69:13:35:ae:48:5f:50:7e:b0:9a:3f:18:44:83:44:58:b6:
         2c:90:29:38:28:fe:54:16:e5:43:0c:1b:fb:5d:9b:a2:dc:17:
         1f:7d:7b:27:81:7b:1f:72:ee:48:4d:f9:72:42:a0:67:50:12:
         2d:22:0f:63:c5:14:5d:60:2c:31:91:4e:dd:2d:e0:e9:e6:be:
         49:df:1b:89:d7:59:24:a7:e8:e3:c5:fa:a7:b7:b4:e6:75:eb:
         4c:a2:e9:bc:d1:43:5f:08:f0:b0:29:fe:1e:16:4f:ab:27:62:
         0f:cf:a6:1c:c3:40:5d:f7:47:63:0c:ff:dc:8d:79:54:b5:88:
         bc:45:64:95:d3:68:52:a6:ba:0d:af:d8:0f:2a:3c:81:eb:24:
         95:25:a7:ac
```

- miramos los server web 1 y 2
> openssl x509 -noout -text -in servercert.web1.pem  
```
Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            9e:bc:e3:24:8e:1b:b4:9b
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Validity
            Not Before: Mar 22 18:53:12 2019 GMT
            Not After : Mar 19 18:53:12 2029 GMT
        Subject: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = www.web1.org, emailAddress = web1@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:c8:ac:bb:98:e7:e3:c7:84:5d:25:f8:74:f0:b1:
                    6f:e3:43:b0:23:2f:19:53:8c:28:f3:e0:e7:e3:f8:
                    04:7f:cd:99:11:49:06:8f:f2:a9:0f:d3:30:31:39:
                    14:fc:11:89:9d:80:ae:3b:0a:49:d2:0d:4d:25:eb:
                    53:53:ec:b7:bd:a5:f2:5b:f0:dc:b1:bb:9e:41:44:
                    f5:7d:fc:ea:98:f6:6f:5f:ef:38:83:3f:2c:4c:c7:
                    a2:e0:69:4a:f8:8c:04:9a:72:f7:f5:67:c2:dc:cb:
                    61:0c:8c:23:87:9e:39:b4:7d:c0:ad:dc:61:24:c0:
                    8d:55:cb:0b:97:df:99:54:cf:c0:45:55:ef:4f:35:
                    31:bb:76:87:23:8d:05:a4:b5:47:9e:7a:33:7e:53:
                    86:0c:bd:3e:ab:4e:12:07:e6:3f:40:dc:04:b9:5e:
                    b6:d6:4d:de:68:f9:03:33:9e:50:3e:53:0c:af:ed:
                    bb:ea:08:da:9d:0d:ce:f2:30:85:48:09:fc:1f:09:
                    98:af:ef:25:23:3b:02:75:a7:be:80:70:77:6c:48:
                    23:ab:25:15:d3:b7:40:1d:03:0b:42:eb:88:57:74:
                    30:0e:c6:18:96:b2:c4:8c:6b:a6:71:53:c6:ba:56:
                    78:ad:f1:59:ab:21:0a:32:7e:e4:7a:19:5e:f6:22:
                    64:8d
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         af:cb:93:a2:80:5f:fe:51:8b:9e:1d:5c:9e:b9:50:d8:e2:2e:
         6b:98:f2:29:ca:9a:e9:7b:ec:ac:0a:32:5f:91:b5:77:e3:7b:
         76:98:d4:8f:8c:c1:43:a5:9a:c2:7c:fa:e1:84:a6:cb:16:22:
         c7:42:a5:7e:af:93:17:0b:88:31:00:ad:1e:1a:61:51:a1:22:
         ba:b2:92:66:b3:79:86:68:ae:dc:c4:b9:fc:85:b7:a3:35:9b:
         9f:bb:ff:81:98:bf:67:9e:eb:f4:5a:a1:2c:88:2e:18:4d:28:
         43:bb:0e:7f:29:ee:d4:ae:7d:8c:48:d5:48:7c:27:a8:dd:79:
         e1:5a:c7:7f:c5:81:87:c4:1f:87:19:60:c3:f8:5f:78:72:e6:
         06:35:65:96:16:e8:bb:4a:33:01:30:24:52:6e:d3:07:e3:ff:
         c0:f9:64:23:80:6c:af:d0:58:0e:90:8e:9b:60:35:8a:9c:29:
         d6:9e:b5:46:e8:fd:13:dd:2f:2c:d8:65:97:59:80:8a:22:b5:
         9e:3f:71:88:8a:03:1f:92:51:0e:61:1a:a4:fb:32:22:04:17:
         2b:77:9b:01:c6:50:5a:ab:ee:5e:d6:7c:b9:34:bf:1d:ee:f1:
         59:86:15:8c:46:c5:c2:58:58:ed:24:f4:21:c2:66:9c:29:c9:
         74:b5:42:ac
```

- miramos el ca veritat absoluta
> openssl x509 -noout -text -in cacert.pem 
```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            b2:29:33:db:cb:f3:92:a3
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Validity
            Not Before: Mar 22 10:39:57 2019 GMT
            Not After : Mar 19 10:39:57 2029 GMT
        Subject: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:f4:d1:a9:dc:4c:55:d9:d5:2c:b1:19:c5:7b:ad:
                    ed:e0:b8:f2:6f:5f:4d:f6:cf:ac:ca:4d:67:a9:bb:
                    08:66:8b:73:21:61:d3:e2:e1:a1:53:a7:45:4c:23:
                    a8:06:e3:57:5d:12:c8:fa:1e:c8:d7:12:0f:8f:fc:
                    dd:3f:f2:f9:a8:24:fb:90:83:d1:a9:37:cc:0e:7c:
                    59:d1:ea:12:d9:ee:ad:e8:42:5e:f1:b4:9d:a2:3a:
                    46:3f:00:54:be:7f:af:fa:c9:7e:4c:f0:61:97:8d:
                    96:d5:25:99:bc:54:36:a3:07:79:5f:95:00:01:3d:
                    22:63:3a:34:0e:c2:41:2f:c2:88:ae:5c:7f:6a:81:
                    b2:ff:ad:05:b4:dc:4e:48:2e:95:f7:04:46:8f:fe:
                    12:2c:ef:a0:01:dd:44:40:d6:44:2b:f6:36:fd:60:
                    80:e5:c5:a7:b9:7d:8d:2d:3d:33:e0:6d:c5:9e:d3:
                    8d:a4:3e:23:ec:43:17:6c:b4:36:56:75:10:95:7a:
                    c5:71:00:97:89:25:55:0d:61:78:cf:da:7a:64:cf:
                    10:5c:bc:a2:62:3f:b9:04:db:8d:b6:d4:b2:09:50:
                    10:07:2a:08:16:bb:30:5a:65:39:2f:d9:d3:e9:90:
                    e8:ce:54:1a:35:dd:d6:a1:d5:f2:83:59:af:bb:1f:
                    ae:03
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                C7:58:F2:1C:98:6E:50:77:C6:7C:4D:2C:AD:39:62:39:7E:DB:72:83
            X509v3 Authority Key Identifier: 
                keyid:C7:58:F2:1C:98:6E:50:77:C6:7C:4D:2C:AD:39:62:39:7E:DB:72:83

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         92:23:1d:b7:d1:94:ca:db:96:d6:5f:74:4e:6f:06:be:8c:29:
         14:f7:72:e8:0e:5f:08:02:99:fe:73:26:e6:9c:8c:91:20:5e:
         2c:36:d5:58:b1:ce:ab:d0:82:42:29:38:a3:ef:f0:0b:2a:28:
         dd:a1:99:33:b9:73:e9:0c:7a:92:54:83:ad:68:a2:19:78:69:
         7e:f9:46:db:45:ce:16:6c:12:a2:17:b7:9b:0b:b1:3c:93:81:
         97:3b:99:61:15:9e:51:7e:6d:3d:74:da:43:85:ce:0d:a4:23:
         77:6f:9c:c6:78:0c:f5:3a:63:81:72:33:2a:3f:a5:5a:25:fa:
         ec:a5:9a:6e:bf:6f:99:18:7e:7d:1d:de:b0:7c:d1:40:5e:fb:
         e2:d8:1f:91:d3:00:63:c7:c7:5e:51:eb:bc:64:51:2a:3b:1a:
         25:59:5a:71:e5:42:f1:ec:dd:f0:41:d2:6e:d5:01:eb:c1:fd:
         68:c2:39:16:7d:7d:c7:b4:f3:cd:98:23:ec:db:be:72:cb:90:
         00:e8:1f:46:c8:ac:3b:48:24:ed:52:7d:aa:a3:04:e6:28:90:
         26:91:e7:51:32:13:77:4e:3f:e2:23:d5:1c:88:86:e0:24:d8:
         c0:be:d7:fa:2e:82:73:ef:aa:3e:5c:e4:c6:7c:ce:1b:db:33:
         b5:13:a5:e6
```
- miramos los **auto** 1 y 2

- agarramos la ip del cont y vamos a un container

- vamos a view certificated

- editamos los hosts del container y de nuestra local
> vim /Etc/hosts
```
172.18.0.2      web.edt.org web web www.auto1.cat www.auto2.cat www.web1.org www.web2.org
```

> ping www._______.______

- copiar cert del docker
> docker cp web.edt.org:/opt/docker/cacert.pem /tmp/cacert.pem

- importa el cert dentro de la configuracion del navegador

- volver a cargar la pag y se ve que no necesitas el cert

- encendemos otra maquina **ldap**
> docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -d edtasixm11/tls18:ldaps

- hacemos una consulta (local)
> ldapsearch -x -h 172.18.0.3 -b 'dc=edt,dc=org'

- editamos nuestro /etc/hosts
> sudo vim /etc/hosts
```
172.18.0.3	ldap.edt.org
```

STARTTLS: 


## Ejemplo TLS_LDAP
- entrar al docker
> docker run --rm --name -h edtasixm11/tls18:ldaps

- probar una consulta
> ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'

- mirar dentro del fichero de conf
> vi slapd.conf

- conectarno a una conexion **insegura**
> ldapsearch -x -LLL -H ldap://ldap.edt.org -b 'dc=edt,dc=org'

- conectarno a una conexion **segura**
> ldapsearch -x -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'

- mira los siguentes ficheros:
  - install
  - startup

> ldapsearch -x -LLL -d1 -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
-d1: mostrar dialogo

> ldapsearch -x -LLL -Z -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
-Z: 
-ZZ: 

en otra terminal, nuestra:
> ldapsearch -x -LLL -h <ip_cont_ldaps> -s base -b 'dc=edt,dc=org'

- editamos nuestro ldap.conf
> vim /etc/ldap/ldap.conf

- copiamos el cert del docker
> docker cp ldap.edt.org:/opt/docker/cacert.pem /tmp/.

- editamos el hosts, local

- hacemos una consulta
> ldapsearch -x -LLL -H ldaps://ldap.edt.org -s base -b 'dc=edt,dc=org'

dentro del container:
-  instalar openssl
> yum install openssl

- 
> openssl x509 -noout -text -in 
 
- hacer consultas:
  > ldapsearch
  > ldapsearch
  > ldapsearch

> openssl s_client -connect 172.17.0.2 < /dev/null 2> /dev/null | openssl x509 -noout -text

> openssl s_client -connect gmail.com:443 < /dev/null 2> /dev/null | openssl x509 -noout -text
```muestra el certificado de google```

# Practica 1: TLS_SSL + LDAP
## Necesario: ldaps
- [x] ficheros del servidor-cliente ldap (ldap:grups)
- [x] montar los certificados de CA (Veritat Absoluta/cualquier otro nombre)
- [x] montar el certificado de Servidor (ldap.conf)
- [x] abrir los puerto de conexion segura (636)

## Verificación. [Aqui](#verificacion)
- [x] validar con 2 alias (IP servidor y loopback)
- [x] poder conectarse al ldap usando el **puerto privilegiado** tambien el puerto ldap (**inseguro**) y usando **starttls** para una conexion **segura**. 
---
1. copiamos los fichero del ldap tanto del servidor como cliente
```
.
├── Dockerfile
├── edt.org.ldif
├── ldap.conf
├── login.defs
├── slapd.conf
└── startup.sh

0 directories, 5 files
```

2. montamos el certificado de entidad de CA
   
   2.1. creamos las clave privada para CA (Entidad de Certificación)
   > openssl genrsa -des3 --out cakey.pem 2048
   
    - **-desc3**: con passphrase (no es necesario, en mi caso, yo lo añado)
    - **2048**: tamaño de las claves

    Verifica contenido de __cakey.pem__
   > openssl rsa --noout --text -in cakey.pem

   2.2. crear el certificado de CA mediante la clave privada
   > openssl req -new -x509 -days 365 -key cakey.pem -out ca_johnwick_cert.pem
    ````
    Country: __CA__
    Province: __Catalunya__
    Locality: __Barcelona__
    Organization Name: __John Wick__
    Organizational Unit: __johnwick__
    Common Name: __john.edt.org__
    Email Address: __jwick@edt.org__
    ````
    Verificar la __ca_johnwick_cert.pem__
    > openssl x509 --noout --text -in ca_johnwick_cert.pem

3. montamos el certificado de Servidor
   
    3.1. creamos la clave privada del Servidor Ldap (sin passphrase)
    > openssl genrsa --out serverkey_ldap.pem 9069

    Verificar contenido de __serverkey_ldap.pem__
    > openssl rsa --noout --text -in serverkey_ldap.pem

    3.2. crear el certificado del servidor ldap
    > openssl req -new -x509 -days 365 -nodes -key serverkey_ldap.pem -out servercert_ldap.pem
    ````
    Country: _CA_
    Province: _Catalunya_
    Locality: _Barcelona_
    Organization Name: _Escola del Treball_
    Organizational Unit: _ldap_
    Common Name: _ldap.edt.org_
    Email Address: _ldap@edt.org_
    ````
    Verificar el __servercert_ldap.pem__
    > openssl req --noout --text -in servercert_ldap.pem

    3.3. crear el request mediante como entidad ldap (clave privada)
    > openssl req -new -key serverkay_ldap.pem -out serverrequest_ldap.pem
    ````
    Country: __CA__
    Province: __Catalunya__
    Locality: __Barcelona__
    Organization Name: __Escola del Treball__
    Organizational Unit: __ldap__
    Common Name: __ldap.edt.org__
    Email Address: __ldap@edt.org__
    
    A challenger password: __jupiter__
    An optional company name: __edt__
    ````
    Verificar el __serverrequest_ldap.pem__ (vemos que esta incluido la peticion de firma)
    > openssl req -noout -text -in serverrequest_ldap.pem

    3.4. firmar, como entidad CA, la request para ldap y **ya tenemos el certificado del ldap completo**
    > openssl x509 -CA ca_johnwick_cert.pem -CAkey cakey.pem -req -in serverrequest_ldap.pem -CAcreateserial -days 365 -out servercert_ldap.pem
    - **-CA**: certificado de entidad CA
    - **-CAkey**: clave priv de CA
    - **-req**: request de la entidad ldap
    - **-CAcreateserial**: crea el certificado de ldap

    Verificar el __servercert_ldap.pem__ (con la entidad CA (**Issuer**) y la entidad LDAP (**Subject**))
    > openssl x509 --noout --text -in servercert_ldap.pem

4. Modificamos el Dockerfile y startup.sh de Ldap
    - Dockerfile
    ````dockerfile
    FROM debian:latest
    LABEL version="1.0"
    LABEL author=" @fercrisjicon ASIX-M11"
    LABEL subject="Ldap Server - LDAPS"
    RUN apt-get update
    ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get install -y procps iproute2 tree nmap vim nano ldap-utils systemd slapd openssl
    RUN mkdir /opt/docker
    COPY * /opt/docker/
    RUN chmod +x /opt/docker/startup.sh
    WORKDIR /opt/docker
    CMD /opt/docker/startup.sh
    EXPOSE 389
    EXPOSE 636
    ````
    - startup.sh
    ````sh
    #! /bin/bash
    # tls22:ldaps
    # @fercrisjicon ASIX M11-SAD Curs 2021-2022

    mkdir /etc/ldap/certs

    # Ficheros de configuracion
    cp /opt/docker/ca_johnwick_cert.pem /etc/ldap/certs/. # para probar que es el mismo
    cp /opt/docker/ca_johnwick_cert.pem /etc/ssl/certs/. # certificado de CA
    cp /opt/docker/servercert_ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
    cp /opt/docker/serverkey_ldap.pem /etc/ldap/certs/. # clave pub/priv del Server

    # Limpieza
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*

    # Database
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
    chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

    # Ficheros de configuracion
    cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

    # Encender servers, en detach
    /usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
    ````

5. Modificar los ficheros de configuracion cliente/servidor de ldap
    - ldap.conf
    ````conf
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE	dc=edt,dc=org
    URI	ldap://ldap.edt.org
    #URI	ldaps://ldap.edt.org #???

    #SIZELIMIT	12
    #TIMELIMIT	15
    #DEREF		never

    # TLS certificates (needed for GnuTLS)

    TLS_CACERT	/etc/ldap/certs/ca_johnwick_cert.pem
    ````
    - slapd.conf
    ````conf
    #
    # See slapd.conf(5) for details on configuration options.
    # This file should NOT be world readable.
    #
    # debian packages: slapd ldap-utils

    include		/etc/ldap/schema/corba.schema
    include		/etc/ldap/schema/core.schema
    include		/etc/ldap/schema/cosine.schema
    include		/etc/ldap/schema/duaconf.schema
    include		/etc/ldap/schema/dyngroup.schema
    include		/etc/ldap/schema/inetorgperson.schema
    include		/etc/ldap/schema/java.schema
    include		/etc/ldap/schema/misc.schema
    include		/etc/ldap/schema/nis.schema
    include		/etc/ldap/schema/openldap.schema
    include		/etc/ldap/schema/ppolicy.schema
    include		/etc/ldap/schema/collective.schema

    # Allow LDAPv2 client connections.  This is NOT the default.
    allow bind_v2

    pidfile		/var/run/slapd/slapd.pid

    # Certificados-Practica ================================
    TLSCACertificateFile        /etc/ldap/certs/ca_johnwick_cert.pem
    TLSCertificateFile		/etc/ldap/certs/servercert_ldap.pem
    TLSCertificateKeyFile       /etc/ldap/certs/serverkey_ldap.pem
    TLSVerifyClient       never
    # =======================================================

    #argsfile	/var/run/openldap/slapd.args

    modulepath /usr/lib/ldap
    moduleload back_mdb.la
    moduleload back_monitor.la

    # ----------------------------------------------------------------------
    database mdb
    suffix "dc=edt,dc=org"
    rootdn "cn=Manager,dc=edt,dc=org"
    rootpw secret
    directory /var/lib/ldap
    index objectClass eq,pres
    access to * 
        by self write
        by * read
    # ----------------------------------------------------------------------
    database monitor
    access to *
        by dn.exact="cn=Manager,dc=edt,dc=org" read
        by * none
    ````

6. montar imagen tls22:ldaps

Antes que nada, asegurate que tu __startup.sh__ tenga permisos de ejecucion.
> docker build -t cristiancondolo21/tls22:ldaps .
---
#### Verificacion
7. encender el container en detach
> docker run --rm --name ldaps.edt.org -h ldaps.edt.org --net 2hisix -d cristiancondolo21/tls22:ldaps

Verificamos que este __en detach__.
> docker ps

8. crear el directorio ``/etc/ldap/certs`` en nuestra local y colocar el certificado de CA
> sudo mkdir /etc/ldap/certs

> sudo cp ca_johnwick_cert.pem /etc/ldap/certs

9.  modificar nuestro ``ldap.conf`` y ``hosts``
    - ldap.conf
    ````
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE	dc=edt,dc=org
    URI	ldap://ldap.edt.org

    #SIZELIMIT	12
    #TIMELIMIT	15
    #DEREF		never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT	/etc/ldap/certs/ca_johnwick_cert.pem
    ````
    - /etc/hosts
    ````
    172.18.0.2  ldap.edt.org
    ````

10. probamos hacer consultas
    
    10.1. mostrar **la base**, con **conexion insegura**.
    > ldapsearch -x -H ldap://ldap.edt.org -s base
    
    <center><img src="Fotos/imagen01.png"/></center>

    10.2. trar **la base**, con **conexion segura**.
    > ldapsearch -x -H ldaps://ldap.edt.org -s base

    <center><img src="Fotos/imagen02.png"/></center>

    10.3. mostrar **el debug**, con **conexion segura** (**leible**).
    > ldapsearch -d1 -x -LLL -H ldaps://ldap.edt.org

    <center><img src="Fotos/imagen03.png"/></center>

    10.4. vuelve una conexion **insegura** a **segura** con **starttls**.
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org
    
    (si da ERROR al iniciar no pasa nada, funciona igualmente)
    
    <center><img src="Fotos/imagen04.png"/></center>

    10.5. forzar a usar **starttls**.
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org -s base

    <center><img src="Fotos/imagen05.png"/></center>

    10.6. Verificamos si van por el puerto **inseguro**.
    > ldapsearch -d1 -x -ZZ -H ldap://ldap.edt.org

    (vemos que va por el puerto inseguro 389)

    <center><img src="Fotos/imagen06.png"/></center>

    10.7. ant. lo guarda en un Log y muestra por pantalla
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org -d1 dn 2> log

    <center><img src="Fotos/imagen07.png"/></center>

    10.8. conexion normal especificando host en vez de URI y base manualmente
    > ldapsearch -x -LLL -h 172.18.0.2 -s base -b 'dc=edt,dc=org'

    <center><img src="Fotos/imagen08.png"/></center>

    10.9. intentar iniciar como **starttls** sin especificar URI y como host,
    > ldapsearch -x -LLL -Z -h 172.18.0.2 -s base -b 'dc=edt,dc=org’

    (da error. no funciona por IP, el certificado solo funciona por CN (Common Name))

    <center><img src="Fotos/imagen09.png"/></center>

    > ldapsearch -x -LLL -H ldaps://172.18.0.2 -s base -b 'dc=edt,dc=org'

    <center><img src="Fotos/imagen10.png"/></center>


 # Teoria
 - clave privada
> openssl rsa --noout -text -in cakey.pem
- certificado firmada
> openssl x509 --noout -text -in cacert.pem

**en caso de no tener:**
- como se fabrica una clave privada
> openssl genrsa -out cakey.pem
- como crear un certif
> openssl req -new -x509 -key cackey.pem -days 365 -out cacert.pem 
    ca
    barcelona
    bcn
    VeritatAbsoluta
    Certificats
    veritat
    veritat@edt.org

- editamos un fichero
> vim alternet.conf
```
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth
subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,URI:ldaps://mysecureldapserver.org
```

- creamos un request
> openss req -newkey rsa:2048 -nodes -keyout keys1.pem -out reqs1.pem
-nodes: no pide passphrase
> openssl req --noout -text -in reqs1.pem

> openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in reqs1.pem -out cert1.pem -CAcreateserial 

> car cacert.srl

> openssl x509 -noout -text -in cert1.pem
no hay extensions

- machacamos el cert
> openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in reqs1.pem -out cert1.pem -CAcreateserial -extfile alternate.conf -days 360

> openssl x509 -noout -text -in cert1.pem
valido por un año (2022->2023)
tiene extensions

- editamos el alternate
> vim alternate.conf
```
,DNS:web,DNS:myweb.org
```

- y volvemos a visualizar 
> openssl x509 -noout -text -in cert1.pem

/etc/pki = public key infrastructure

- editamos un fichero
> vim exemple1.cfg
```
[ my_client ]
basicConstraints= CA:FALSE
subjectKeyIdentifier= hash
authorityKeyIdentifier  = keyid,issuer:always

[ my_server ]
basicConstraints= CA:FALSE
nsCertType= server
nsComment= "OpenSSL Generated Server Certificate"
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
extendedKeyUsage= serverAuth
keyUsage= digitalSignature, keyEncipherment

[ my_edt ]
basicConstraints= CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
keyUsage = cRLSign, keyCertSign
```
> openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in reqs1.pem -out cert1.pem -CAcreateserial -extfile exemple1.cfg -extensions my_client -days 360 
> #openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in reqs1.pem -out cert1.pem -CAcreateserial -extfile exemple1.cfg -extensions my_server -days 360 
> #openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in reqs1.pem -out cert1.pem -CAcreateserial -extensions v3_ca -days 360 

- copiamos el openssl.cfg
> cp /etc/ssl/openssl.cfg

- modificamos el fichero
    - donde estan los fichero
    - configuracion global
probamos
```
default_days = 3650
```

- creamos ficheros/directorios
> mkdir certs newcerts private crl
> mv cakey.pem private/
> vim serial
```
01
```
> touch index.txt
> mkdir merda
> mv alternate.conf cert1.pem keys1.pem exemple1.cfg reqs1.pem merda/

- editar openssl.cfg
```
[CA_default]
dir     = .

[req_distindigue_name]
....
```

> openssl req -new -config openssl.cnf -key merda/keys1.pem -out merda/req.pem
```vemos los cambios añadidos```

- editamos de nuevo openssl.cnf
```
policy      = policy_anything
```

> openssl ca -in merda/req.pem -config openssl.cnf -out certs.pem
```
respondemos yes a las dos preguntas
Write out database with 1 new entries
Data Base Updated
```

nuevos ficheros:
```
-rw-r--r-- 1 isx41016667 hisx2    93 Apr  6 09:09 index.txt
-rw-r--r-- 1 isx41016667 hisx2    21 Apr  6 09:09 index.txt.attr
-rw-r--r-- 1 isx41016667 hisx2     0 Apr  6 08:57 index.txt.old
```

> cat index.txt
```
V	320403070928Z		01	unknown	/C=CA/ST=Barcelona/L=Santaco/O=Escola del Treball/OU=edt/CN=papa
```

> cat serial
```
02
```

ha añadido nuevos ficheros
> tree
```
├── newcerts
│   └── 01.pem
```
> openssl x509 -noout -text -in newcerts/01.pem
```
Serial Number: 1
```

- porbar a crear de nuevo el cert
> openssl ca -in merda/req.pem -config openssl.cnf -out certs.pem
```
Using configuration from openssl.cnf
Check that the request matches the signature
Signature ok
ERROR:There is already a certificate for /C=CA/ST=Barcelona/L=Santaco/O=Escola del Treball/OU=edt/CN=papa
The matching entry has the following details
Type          :Valid
Expires on    :320403070928Z
Serial Number :01
File name     :unknown
Subject Name  :/C=CA/ST=Barcelona/L=Santaco/O=Escola del Treball/OU=edt/CN=papa
```
- US
- oklahoma

- cia
- cia
- bin laden

> openssl req -new -config openssl.cnf -key merda/keys1.pem -out merda/req3.pem
> openssl ca -in merda/req3.pem -config openssl.cnf -extensions my_edt -out certs2.pem

> cat serial
```
03
```
> cat index.txt
```
V	320403070928Z		01	unknown	/C=CA/ST=Barcelona/L=Santaco/O=Escola del Treball/OU=edt/CN=papa
V	320403072817Z		02	unknown	/C=CA/ST=Barcelona/L=Santaco/O=Escola del Treball/CN=inf
```

## Utilitats s_client, s_server, ncat
## Client / Servidor ncat
> ncat --ssl -k -l 8080

- atacando el servidor pop de gmail
> openssl s_client -connect pop.gmail.com:995
```
USER fercrisjicon <- tu user de gmail
+OK send PASS
```

> telnet pop.gmail.com 25
```
EHLO localhost
250-smtp.gmail.com at your service, [2.136.113.129]
250-SIZE 35882577
250-8BITMIME
250-STARTTLS
250-ENHANCEDSTATUSCODES
250-PIPELINING
250-CHUNKING
250 SMTPUTF8
STARTTLS
^C
```

> openssl s_client -connect imap.gmail.com:993

> openssl s_server -cert merda/cert1.pem -key merda/keys1.pem -www -accept 50000

- ir a un navegador:
  - https://localhost:50000/
  - aceptar el riesgo
  - ir al simbolo del candado
  - ver certificado

---

### Cosas por ver
##### - Diffie-Hellman key exchange**
https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange

Alice & Bob intentan comunicarse.
- Common paint
- Secret colours
- Public transport
- Secret colours
- Common secret

---


















