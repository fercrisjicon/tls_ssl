
#### Cristian Condolo
#### EDT ASIX2 2021-2022
#### M11

# **INDICE**

==================================================================================

+ **tls22:ldap**: imagen de un LDAP Server/Client normal con BD (base de datos). [--> readME <--](#montar-un-servidor-ldap-normal-tls22ldap)

+ **tls22:ldaps**: imagen de un LDAP Server/Client con certificados OpenSSL. [ --> readME <--](#montar-un-servidor-ldap-con-certificados-openssl-tls22ldaps)

+ **tls22:vpn**: config files de un servidor VPN con nuestros certificados. [ --> readME <--](#montar-un-servidor-vpn-con-nuestro-certificados-en-lugar-de-los-por-defecto-tls22vpn)

==================================================================================

# **CHULETON IMPOSIBLE**

Lo recomendable es ir paso a paso para liarse pro el camino.

Primero montaremos un servidor __LDAP__ normal, *sin certificados* *ni claves privadas*.

> **Nota**: recomiendo que se vaya comprobando cada cambio que se haga en los ficheros de configuracion de algun servidor, al final.

## **Montar un servidor ldap normal (tls22:ldap)**

1. Copiar los file conf del ldap server/client de alguna practica ant.

![](./Fotos/ldap_server/ldap01.png)

2. Editar todos los files del ldap server/client

- **Dockerfile**
```Dockerfile
FROM debian:11
LABEL version="1.0"
LABEL author="@fercrisjicon ASIX-M11"
LABEL subject="TLS Ldap Server cn dataDB"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap nano vim systemd slapd ldap-u>
RUN mkdir /opt/docker
COPY . /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
EXPOSE 636
```

- **starup.sh**
```bash
#! /bin/bash
# tls22:ldaps
# @fercrisjicon ASIX M11-SAD Curs 2021-2022
#mkdir /etc/ldap/certs

# Ficheros de configuracion
#cp /opt/docker/ca_johnwick_cert.pem /etc/ldap/certs/. # para probar que es el mismo
#cp /opt/docker/ca_johnwick_cert.pem /etc/ssl/certs/. # certificado de CA
#cp /opt/docker/servercert_ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
#cp /opt/docker/serverkey_ldap.pem /etc/ldap/certs/. # clave pub/priv del Server

# Limpieza
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*

cp /opt/docker/slapd.conf /etc/ldap/slapd.conf

# Database
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slaptest -u -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Ficheros de configuracion
cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

# Encender servers, en detach
/usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
```
Solo comentamos la copia de los certificados, para mas adelante, y solo injectamos la base de datos ``edt.org.ldif`` y los files conf del ldap server.

- **ldap.conf**
```bash
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE   dc=example,dc=com
#URI    ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT      12
#TIMELIMIT      15
#DEREF          never

# TLS certificates (needed for GnuTLS)
TLS_CACERT      /etc/ssl/certs/ca-certificates.crt
#TLS_CACERT     /etc/ssl/certs/ca_johnwick_cert.pem

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```
Solo dejar comentado la copia del certificado CA, para mas adelante, y dejar el certificado por defecto

- **slapd.conf**
```bash
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
include         /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid

# Certificados-Practica ================================
#TLSCACertificateFile        /etc/ldap/certs/ca_johnwick_cert.pem
#TLSCertificateFile             /etc/ldap/certs/servercert_ldap.pem
#TLSCertificateKeyFile       /etc/ldap/certs/serverkey_ldap.pem
#TLSVerifyClient       never
#=======================================================

#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to *
        by self write
        by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```
Solo dejar comentado la copia de certificados y clave para mas adelante.

3. Ahora tocaria montar la imagen mediante los files ant.

``docker build -t cristiancondolo21/tls22:ldap .``

4. Encender el docker/cont en detach

``docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d cristiancondolo21/tls22:ldap``

Comprobar si el docker/cont esta encendido: ``docker ps``

![](./Fotos/ldap_server/ldap02.png)

5. __Desde afuera__, editar los files conf de __ldap client__ para conectarnos al __ldap server__

- **``/etc/ldap/ldap.conf``**
```bash
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#SIZELIMIT      12
#TIMELIMIT      15
#DEREF          never

# TLS certificates (needed for GnuTLS)
TLS_CACERT      /etc/ssl/certs/ca-certificates.crt
#TLS_CACERT     /etc/ssl/certs/ca_johnwick_cert.pem

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```
Dejar comentado la copia del certificado CA para mas adelente

- **``/etc/hosts``**
```bash
172.18.0.2 ldap.edt.org
```
Añadimos la IP del docker/cont con su hostname al lado para no tener que llamarlo de su IP.

6. __Desde afuera__, comprobamos que funciones tanto el __ldap client__ como el __ldap server__

``ldapsearch -x -LLL -s base``

![](./Fotos/ldap_server/ldap03.png)

## **Montar un servidor ldap con certificados openssl (tls22:ldaps)**

> **Nota**: se recomienda para el docker/cont ant para que no interfiera en la siguiente practica: ``docker stop ldap.edt.org``

1. Editar todos los files del ldap server/client y una extension para añadir un nombre de dominio

- startup.sh
```bash
#! /bin/bash
# tls22:ldaps
# @fercrisjicon ASIX M11-SAD Curs 2021-2022
mkdir /etc/ldap/certs

# Ficheros de configuracion
cp /opt/docker/ca_veritatabsoluta_cert.pem /etc/ldap/certs/. # para probar que es el mismo
cp /opt/docker/ca_veritatabsoluta_cert.pem /etc/ssl/certs/. # certificado de CA
cp /opt/docker/servercert.ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
cp /opt/docker/serverkey.ldap.pem /etc/ldap/certs/. # clave pub/priv del Server

# Limpieza
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*

cp /opt/docker/slapd.conf /etc/ldap/slapd.conf

# Database
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slaptest -u -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Ficheros de configuracion
cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

# Encender servers, en detach
/usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
```

- ldap.conf
```bash
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE   dc=example,dc=com
#URI    ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT      12
#TIMELIMIT      15
#DEREF          never

# TLS certificates (needed for GnuTLS)
#TLS_CACERT     /etc/ssl/certs/ca-certificates.crt
TLS_CACERT      /etc/ssl/certs/ca_veritatabsoluta_cert.pem

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```

- slapd.conf
```bash
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include         /etc/ldap/schema/corba.schema
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/duaconf.schema
include         /etc/ldap/schema/dyngroup.schema
include         /etc/ldap/schema/inetorgperson.schema
include         /etc/ldap/schema/java.schema
include         /etc/ldap/schema/misc.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/openldap.schema
include         /etc/ldap/schema/ppolicy.schema
include         /etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile         /var/run/slapd/slapd.pid

# Certificados-Practica ================================
TLSCACertificateFile        /etc/ldap/certs/ca_veritatabsoluta_cert.pem
TLSCertificateFile              /etc/ldap/certs/servercert.ldap.pem
TLSCertificateKeyFile       /etc/ldap/certs/serverkey.ldap.pem
TLSVerifyClient       never
#=======================================================

#argsfile       /var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to *
        by self write
        by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

- myextention.cnf
```bash
[ my_ext ]

# Extensions to add to a certificate request

basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]

DNS.0 = veritat.edt.org
DNS.1 = mysecureldapserver.org
DNS.2 = ldap
IP.1 = 172.18.0.2
IP.2 = 172.0.0.1
```

Hay que copiarlo de la plantilla de openssl. Y añadimos estas lineas.

### **Generar las claves y certificado para la CA (VeritatAbsoluta)**

2. Generar la __clave privada__ simple

``openssl genrsa -out cakey.pem 1024``

![](./Fotos/ldaps_server/ldaps01.png)

3. Fabricar el __certificado__ autofirmado __de Veritat Absoluta__

``openssl req -new -x509 -nodes -sha1 -days 365 -key cakey.pem -out ca_veritatabsoluta_cert.pem``

![](./Fotos/ldaps_server/ldaps02.png)

### **Generar las claves y certifiacados para el servidor Ldap**

4. Generar una '__request__' para el servidor Ldap y genera la __clave privada__ del servidor, mediante el file __myextention.cnf__.

``openssl req -newkey rsa:2048 -sha256 -keyout serverkey_ldap.pem -out serverrequest_ldap.pem -config myextention.cnf``

![](./Fotos/ldaps_server/ldaps04.png)

Hacemos __la peticion__ (__de firma__) a la CA (__VeritatAbsoluta__), nos preguntara *quines somos?*

5. __Firmar la__ '__request__' enviada por Ldap: se genera el cacert.pem

``openssl x509 -CA ca_veritatabsoluta_cert.pem -CAkey cakey.pem -req -in serverrequest.ldap.pem -days 3650 -CAcreateserial -out servercert.ldap.pem -extensions 'aaron_req' -extfile extserver.cnf``

![](./Fotos/ldaps_server/ldaps06.png)

1. Montamos la imagen y la encendemos en detach

> Nota: vigila los permisos de todos los ficheros, en general seria: ``sudo chmod 664 *`` pero para el __startup.sh__ es abrir los permisos de ejecución: ``sudo chmod 775 startup.sh``

```
docker build -t cristiancondolo21/tls22:ldaps .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -d cristiancondolo21/tls22:ldaps
docker ps       
```

![](./Fotos/ldaps_server/ldaps07.png)

9. Desde afuera, configurar los files conf del __client ldap__

- **``/etc/ldap/ldap.conf``**
```bash
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE   dc=example,dc=com
#URI    ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT      12
#TIMELIMIT      15
#DEREF          never

# TLS certificates (needed for GnuTLS)
#TLS_CACERT     /etc/ssl/certs/ca-certificates.crt
TLS_CACERT      /etc/ssl/certs/ca_veritatabsoluta_cert.pem

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```
Copiamos el certificado CA al dir indicado en la configuración

- **``/etc/hosts``**
```bash
172.18.0.2 ldap.edt.org mysecureldapserver.org
```

10. __Desde afuera__, comprobar el funcionamiento del servidor

```
ldapsearch -x -LLL -s base
ldapsearch -x -ZZ -LLL -H ldap://ldap.edt.org -b 'dc=edt,dc=org'
ldapsearch -x -LLL -H ldaps://ldap.edt.org -b 'dc=edt,dc=org'
ldapsearch -x -LLL -H ldaps://mysecureldapserver.org -b 'dc=edt,dc=org'
```

![](./Fotos/ldaps_server/ldaps08.png) ![](./Fotos/ldaps_server/ldaps09.png) ![](./Fotos/ldaps_server/ldaps10.png)

Proba a añadir ``-d1`` para ver los errores.
Si consultamos al ldap con una conexion insegura (ldap), utiliza el puerto por defecto __389__.

![](./Fotos/ldaps_server/ldaps11.png)

Si consultamos al ldap con una conexion segura (ldaps), utiliza el puerto seguro __636__.

![](./Fotos/ldaps_server/ldaps12.png)

## **Montar un servidor VPN con nuestro certificados en lugar de los por defecto (tls22:vpn)**

Necesitaremos una maquina AWS, como servidor OpenVPN, con los puertos (22 y 1194) desplegados.

**Dentro del servidor:**

Tengamos a mano un file conf de extencion para el certificado: __ext.server.conf__.

```js
basicConstraints       = CA:FALSE
nsCertType             = server
nsComment              = "OpenVPN Generated Server Certificate"
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid,issuer:always
extendedKeyUsage       = serverAuth
keyUsage               = digitalSignature, keyEncipherment
```

Tener a mano el file __server.conf__.

Podemos reutilizar la clave privada y certificado del CA (Veritat Absoluta).

Generamos la clave privada para el servidor VPN:

``openssl genrsa -out serverkey.vpn.pem 2048``

Y generamos la '__request__':

``openssl req -new -key serverkey.vpn.pem -out serverreq.vpn.pem``

```
Country Name (2 letter code) [AU]:CA
State or Province Name (full name) [Some-State]:Barcelona
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:vpn
Common Name (e.g. server FQDN or YOUR name) []:vpn.edt.org
Email Address []:vpn@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:vpn
```

Firmamos y creamos el __certificado__ para __VPN__:

``openssl x509 -CAkey ../cakey.pem -CA ../ca_veritatabsoluta_cert.pem -req -in serverreq.vpn.pem -days 3650 -CAcreateserial -extfile ext.server.conf -out servercert.vpn.pem``

Creamos la clave DH, se exige para hacer el tunel VPN:

``openssl dhparam -out dh2048.pem 2048``

> **Nota**: Genere claves Diffie-Hellman utilizadas para el intercambio de claves durante el protocolo de enlace TLS entre el servidor OpenVPN y los clientes que se conectan.

Copiamos lo anterior dentro del servidor (AWS):

``sudo scp -i ~/.ssh/cristiancondolo22.pem * admin@[IP-AWS]:/var/tmp``

Nos copiamos los archivos a las carpetas correspondiente (tanto claves como files conf del servidor):

```
sudo cp /var/tmp/server.conf /etc/openvpn/ # Despues de modificarlo mas adelante

sudo cp /var/tmp/ca_veritatabsoluta_cert.pem /etc/openvpn/server/
sudo cp /var/tmp/dh2048.pem /etc/openvpn/server/
sudo cp /var/tmp/ext.client.conf /etc/openvpn/server/
sudo cp /var/tmp/ext.server.conf /etc/openvpn/server/ 
sudo cp /var/tmp/servercert.vpn.pem /etc/openvpn/server/ 
sudo cp /var/tmp/serverkey.vpn.pem /etc/openvpn/server/ 
sudo cp /var/tmp/serverreq.vpn.pem /etc/openvpn/server/
sudo cp /var/tmp/ta.key /etc/openvpn/server/ # Despues de generalo mas adelante

sudo cp /var/tmp/openvpn@.service /etc/systemd/system/ # Despues de modificarlo mas adelante
```

Cambiamos la conf de server.conf. El path de los certificados y claves. Y descomentar la linea __client-to-client__, necesario para tener visibilidad a extremos del tunel:

```
ca /etc/openvpn/server/ca_veritatabsoluta_cert.pem
cert /etc/openvpn/server/servercert.vpn.pem
key /etc/openvpn/server/serverkey.vpn.pem
dh /etc/openvpn/server/dh2048.pem

client-to-client
```

Generar TA Key, dentro de /etc/openvpn/server/:

``sudo openvpn --genkey --secret ta.key``

Encendemos el servidor OpenVPN (encender y apagar):

> openvpn@[nombre_conf].service

```
sudo systemctl stop openvpn@server.service

sudo systemctl start openvpn@server.service
```

Para comprobar que servidor VPN va bien:

``ps -ax | grep openvpn``

Vemos como se creo la interfaz virtual del tunel: ``ip a``

``ip address show tun0``

Comprobar la conectividad del tunel:

``ping 10.8.0.6 [IP Cliente]``

Para comprobar con __xinetd__, copiar el daytime a dentro del dir /etc/systemd.d/:

``cp daytime /etc/systemd.d/xinetd/``

Desde un cliente:

``telnet 10.8.0.10 [IP Cliente] 13``

**Dentro del cliente1:**

Tengamos preparados un file conf de extencion para el certificado: __ext.client.conf__.

```js
basicConstraints        = CA:FALSE
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid,issuer:always
```

Un file conf para clientes VPN:

```bash
client

dev tun

proto udp

remote [IP-AWS] 1194

nobind

persist-key
persist-tun

# # HEM DE CAMBIAR 
ca /etc/openvpn/client/ca_veritatabsoluta_cert.pem
cert /etc/openvpn/client/clientcert.2vpn.pem
key /etc/openvpn/client/clientkey.2vpn.pem

remote-cert-tls server

tls-auth /etc/openvpn/ta.key 1

--data-ciphers-fallback AES-256-CBC

verb 3
```

Y el file conf del servidor OpenVPN: 

```js
[Unit]
Description=OpenVPN Robust And Highly Flexible Tunneling Application On %I
After=network.target

[Service]
PrivateTmp=true
Type=forking
PIDFile=/var/run/openvpn/%i.pid
ExecStart=/usr/sbin/openvpn --daemon --writepid /run/openvpn/%i.pid --cd /etc/openvpn/ --config %i.conf

[Install]
WantedBy=multi-user.target
```

Generamos la clave privada simple para el cliente:

``openssl genrsa -out clientkey.1vpn.pem``

Con la clave privada, firmaso la '__request__':

``openssl req -new -key clientkey.1vpn.pem -out clientreq.1vpn.pem``

```
Country Name (2 letter code) [AU]:CA
State or Province Name (full name) [Some-State]:Barcelona
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:client1
Common Name (e.g. server FQDN or YOUR name) []:client1.edt.org
Email Address []:client1@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:client1
```

Firmamos el certificado utilizando la extension del cliente:

``openssl x509 -CAkey ../cakey.pem -CA ../cacert.pem -req -in clientreq.1vpn.pem -days 3650 -CAcreateserial -extfile ../server/ext.client.conf -out clientcert.1vpn.pem``

Metemos __openvpn@.service__ dentro del dir /etc/systemd/system:

``sudo cp openvpn@.service /etc/systemd/system/.``

Damos los permisos al __ta.key__:

``sudo chmod 600 ta.key``

Copiamos los siguientes archivos:

```
sudo cp client.conf /etc/openvpn/

sudo cp ca_veritatabsoluta_cert.pem clientcert.1vpn.pem clientkey.1vpn.pem clientreq.1vpn.pem /etc/openvpn/client/

sudo cp ta.key /etc/openvpn/
```

Editamos el file conf de client.conf:

```
remote [IP_AWS] 1143
```

Encendemos el servidor:

``sudo systemctl start openvpn@client.service``

Para comprobar la conectividad de los clientes:

``ping [IP_client2]``

Teniendo el puerto daytime (13) funcionando:

```
sudo systemctl start xinetd
nmap localhost
```

Del cliente2 al cliente1:

``telnet [IP_client1] 13``

**Dentro del cliente2:**

Tengamos preparados un file conf de extencion para el certificado: __ext.client.conf__.

```js
basicConstraints        = CA:FALSE
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid,issuer:always
```

Un file conf para clientes VPN:

```bash
client

dev tun

proto udp

remote [IP-AWS] 1194

nobind

persist-key
persist-tun

# # HEM DE CAMBIAR 
ca /etc/openvpn/client/ca_veritatabsoluta_cert.pem
cert /etc/openvpn/client/clientcert.2vpn.pem
key /etc/openvpn/client/clientkey.2vpn.pem

remote-cert-tls server

tls-auth /etc/openvpn/ta.key 1

--data-ciphers-fallback AES-256-CBC

verb 3
```

Y el file conf del servidor OpenVPN: 

```js
[Unit]
Description=OpenVPN Robust And Highly Flexible Tunneling Application On %I
After=network.target

[Service]
PrivateTmp=true
Type=forking
PIDFile=/var/run/openvpn/%i.pid
ExecStart=/usr/sbin/openvpn --daemon --writepid /run/openvpn/%i.pid --cd /etc/openvpn/ --config %i.conf

[Install]
WantedBy=multi-user.target
```

Generamos la clave privada simple para el cliente:

``openssl genrsa -out clientkey.2vpn.pem``

Con la clave privada, firmaso la '__request__':

``openssl req -new -key clientkey.2vpn.pem -out clientreq.2vpn.pem``

```
Country Name (2 letter code) [AU]:CA
State or Province Name (full name) [Some-State]:Barcelona
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:client2
Common Name (e.g. server FQDN or YOUR name) []:client2.edt.org
Email Address []:client2@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:client2
```

Firmamos el certificado utilizando la extension del cliente:

``openssl x509 -CAkey ../cakey.pem -CA ../cacert.pem -req -in clientreq.2vpn.pem -days 3650 -CAcreateserial -extfile ../server/ext.client.conf -out clientcert.2vpn.pem``

Metemos __openvpn@.service__ dentro del dir /etc/systemd/system:

``sudo cp openvpn@.service /etc/systemd/system/.``

Damos los permisos al __ta.key__:

``sudo chmod 600 ta.key``

Copiamos los siguientes archivos:

```
sudo cp client.conf /etc/openvpn/

sudo cp cacert.pem clientcert.2vpn.pem clientkey.2vpn.pem clientreq.2vpn.pem /etc/openvpn/client/

sudo cp ta.key /etc/openvpn/
```

Editamos el file conf de client.conf:

```
remote [IP_AWS] 1143
```

Encendemos el servidor:

``sudo systemctl start openvpn@client.service``

Para comprobar la conectividad de los clientes:

``ping [IP_client1]``

Teniendo el puerto daytime (13) funcionando:

```
sudo systemctl start xinetd
nmap localhost
```

Del cliente1 al cliente2:

``telnet [IP_client2] 13``

#### Regresar al [INDICE](https://gitlab.com/fercrisjicon/tls_ssl/-/tree/main/prueba#indice)