# Practica : OpenVPN amb certificats propis
## Necesario: AWS EC2 + OpenVPN
- [x] instalar openvpn y xinetd
- [x] copiar los ficheros de configuracion
- [ ] modificar para cambiar los certificados por defecto por otros certificados propios
- [ ] Certificados: 1 server (Test-Server) + 2 clientes (Test-Client1 y Test-Client2)
- [ ] Incorporar extenciones para el certificado del servidor y clientes, y valido para OpenVPN
- [ ] montarlo todo, de nuevo, en AWS que permita trafico UDP por el puerto 1194

## Verificacion. [Aqui](#verificacion)
- [ ] verifica el servicio de red: daytime (13)
- [ ] verifica el servicio de red: echo (7)
- [ ] verifica el servicio de red: web (80)

---
1. instalar openvpn y xinetd
> sudo apt-get update

> sudo apt-get install openvpn xinetd

2. copiar ficheros de configuracion de OpenVPN
> cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf sample.server.conf

> cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf sample.client.conf
```
.
├── PracticaOpenVPN.md
├── sample.client.conf
└── sample.server.conf

0 directories, 3 files
```

3. modificar ficheros de configuracion de OpenVPN
    
    3.1. modificar ficheros vpn
    
    - ext.server.conf
    > cp sample.server.conf ext.server.conf
    > vim ext.server.conf
    ```
    ```
    - ext.client.conf
    > cp sample.client.conf ext.client.conf
    > vim ext.client.conf
    ```
    ```

    3.2. modifica ficheros xinetd (dentro de `/etc/xinetd.d/`)
    - echo
    ```asciidoc
    # default: off
    # description: An xinetd internal service which echo's characters back to
    # clients.
    # This is the tcp version.
    service echo
    {
        disable		= no
        type		= INTERNAL
        id		= echo-stream
        socket_type	= stream
        protocol	= tcp
        user		= root
        wait		= no
    }

    # This is the udp version.
    service echo
    {
        disable		= yes
        type		= INTERNAL
        id		= echo-dgram
        socket_type	= dgram
        protocol	= udp
        user		= root
        wait		= yes
    }
    ```
    - daytime
    ```asciidoc
    # default: off
    # description: An internal xinetd service which gets the current system time
    # then prints it out in a format like this: "Wed Nov 13 22:30:27 EST 2002".
    # This is the tcp version.
    service daytime
    {
        disable		= yes
        type		= INTERNAL
        id		= daytime-stream
        socket_type	= stream
        protocol	= tcp
        user		= root
        wait		= no
    }

    # This is the udp version.
    service daytime
    {
        disable		= no
        type		= INTERNAL
        id		= daytime-dgram
        socket_type	= dgram
        protocol	= udp
        user		= root
        wait		= yes
    }
    ```

4. crear certificados propios

5. incorporar extenciones

6. crear un AWS (puerto 1194, UDP) y montar la imagen 

#### Verificacion