# Practica 1: TLS_SSL + LDAP

## Necesario: ldaps

- [x] ficheros del servidor-cliente ldap (ldap:grups)
- [x] montar los certificados de CA (Veritat Absoluta/cualquier otro nombre)
- [x] montar el certificado de Servidor (ldap.conf)
- [x] abrir los puerto de conexion segura (636)

## Verificación. [Aqui](#verificacion)

- [x] validar con 2 alias (IP servidor y loopback)
- [x] poder conectarse al ldap usando el **puerto privilegiado** tambien el puerto ldap (**inseguro**) y usando **starttls** para una conexion **segura**.

---

1. copiamos los fichero del ldap tanto del servidor como cliente

```
.
├── Dockerfile
├── edt.org.ldif
├── ldap.conf
├── login.defs
├── slapd.conf
└── startup.sh

0 directories, 5 files
```

2. montamos el certificado de entidad de CA

   2.1. creamos las clave privada para CA (Entidad de Certificación)
   > openssl genrsa -des3 --out cakey.pem 2048

    - **-desc3**: con passphrase (no es necesario, en mi caso, yo lo añado)
    - **2048**: tamaño de las claves

    Verifica contenido de **cakey.pem**
   > openssl rsa --noout --text -in cakey.pem

   2.2. crear el certificado de CA mediante la clave privada
   > openssl req -new -x509 -days 365 -key cakey.pem -out ca_johnwick_cert.pem

    ````
    Country: __CA__
    Province: __Catalunya__
    Locality: __Barcelona__
    Organization Name: __John Wick__
    Organizational Unit: __johnwick__
    Common Name: __john.edt.org__
    Email Address: __jwick@edt.org__
    ````

    Verificar la **ca_johnwick_cert.pem**
    > openssl x509 --noout --text -in ca_johnwick_cert.pem

3. montamos el certificado de Servidor

    3.1. creamos la clave privada del Servidor Ldap (sin passphrase)
    > openssl genrsa --out serverkey_ldap.pem 9069

    Verificar contenido de **serverkey_ldap.pem**
    > openssl rsa --noout --text -in serverkey_ldap.pem

    3.2. crear el certificado del servidor ldap
    > openssl req -new -x509 -days 365 -nodes -key serverkey_ldap.pem -out servercert_ldap.pem

    ````
    Country: _CA_
    Province: _Catalunya_
    Locality: _Barcelona_
    Organization Name: _Escola del Treball_
    Organizational Unit: _ldap_
    Common Name: _ldap.edt.org_
    Email Address: _ldap@edt.org_
    ````

    Verificar el **servercert_ldap.pem**
    > openssl req --noout --text -in servercert_ldap.pem

    3.3. crear el request mediante como entidad ldap (clave privada)
    > openssl req -new -key serverkay_ldap.pem -out serverrequest_ldap.pem

    ````
    Country: __CA__
    Province: __Catalunya__
    Locality: __Barcelona__
    Organization Name: __Escola del Treball__
    Organizational Unit: __ldap__
    Common Name: __ldap.edt.org__
    Email Address: __ldap@edt.org__
    
    A challenger password: __jupiter__
    An optional company name: __edt__
    ````

    Verificar el **serverrequest_ldap.pem** (vemos que esta incluido la peticion de firma)
    > openssl req -noout -text -in serverrequest_ldap.pem

    3.4. firmar, como entidad CA, la request para ldap y **ya tenemos el certificado del ldap completo**
    > openssl x509 -CA ca_johnwick_cert.pem -CAkey cakey.pem -req -in serverrequest_ldap.pem -CAcreateserial -days 365 -out servercert_ldap.pem
    - **-CA**: certificado de entidad CA
    - **-CAkey**: clave priv de CA
    - **-req**: request de la entidad ldap
    - **-CAcreateserial**: crea el certificado de ldap

    Verificar el **servercert_ldap.pem** (con la entidad CA (**Issuer**) y la entidad LDAP (**Subject**))
    > openssl x509 --noout --text -in servercert_ldap.pem

    3.5. creamos un extencion para fichero conf del openssl (extserver.cnf)

    ```cnf
    [ practica_req ]

    # Extensions to add to a certificate request
    basicConstraints = CA:FALSE
    keyUsage = nonRepudiation, digitalSignature, keyEncipherment
    subjectAltName = @alt_names

    [ alt_names ]
    DNS.0 = aaron.edt.org
    DNS.1 = mysecureldapserver.org
    DNS.2 = ldap
    IP.1 = [IPDocker]
    IP.2 = 127.0.0.1
    ```

4. Modificamos el Dockerfile y startup.sh de Ldap
    - Dockerfile

    ````dockerfile
    FROM debian:latest
    LABEL version="1.0"
    LABEL author=" @fercrisjicon ASIX-M11"
    LABEL subject="Ldap Server - LDAPS"
    RUN apt-get update
    ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get install -y procps iproute2 tree nmap vim nano ldap-utils systemd slapd openssl
    RUN mkdir /opt/docker
    COPY * /opt/docker/
    RUN chmod +x /opt/docker/startup.sh
    WORKDIR /opt/docker
    CMD /opt/docker/startup.sh
    EXPOSE 389
    EXPOSE 636
    ````

    - startup.sh

    ````sh
    #! /bin/bash
    # tls22:ldaps
    # @fercrisjicon ASIX M11-SAD Curs 2021-2022

    mkdir /etc/ldap/certs

    # Ficheros de configuracion
    cp /opt/docker/ca_johnwick_cert.pem /etc/ldap/certs/. # para probar que es el mismo
    cp /opt/docker/ca_johnwick_cert.pem /etc/ssl/certs/. # certificado de CA
    cp /opt/docker/servercert_ldap.pem /etc/ldap/certs/. # certificado de Server Ldap
    cp /opt/docker/serverkey_ldap.pem /etc/ldap/certs/. # clave priv del Server

    # Limpieza
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*

    # Database
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
    chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

    # Ficheros de configuracion
    cp /opt/docker/ldap.conf  /etc/ldap/ldap.conf

    # Encender servers, en detach
    /usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
    ````

5. Modificar los ficheros de configuracion cliente/servidor de ldap y `openssl.cnf`
    - ldap.conf

    ````conf
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE dc=edt,dc=org
    URI ldap://ldap.edt.org
    #URI ldaps://ldap.edt.org #???

    #SIZELIMIT 12
    #TIMELIMIT 15
    #DEREF  never

    # TLS certificates (needed for GnuTLS)

    TLS_CACERT /etc/ldap/certs/ca_johnwick_cert.pem
    ````

    - slapd.conf

    ````conf
    #
    # See slapd.conf(5) for details on configuration options.
    # This file should NOT be world readable.
    #
    # debian packages: slapd ldap-utils

    include  /etc/ldap/schema/corba.schema
    include  /etc/ldap/schema/core.schema
    include  /etc/ldap/schema/cosine.schema
    include  /etc/ldap/schema/duaconf.schema
    include  /etc/ldap/schema/dyngroup.schema
    include  /etc/ldap/schema/inetorgperson.schema
    include  /etc/ldap/schema/java.schema
    include  /etc/ldap/schema/misc.schema
    include  /etc/ldap/schema/nis.schema
    include  /etc/ldap/schema/openldap.schema
    include  /etc/ldap/schema/ppolicy.schema
    include  /etc/ldap/schema/collective.schema

    # Allow LDAPv2 client connections.  This is NOT the default.
    allow bind_v2

    pidfile  /var/run/slapd/slapd.pid

    # Certificados-Practica ================================
    TLSCACertificateFile        /etc/ldap/certs/ca_johnwick_cert.pem
    TLSCertificateFile  /etc/ldap/certs/servercert_ldap.pem
    TLSCertificateKeyFile       /etc/ldap/certs/serverkey_ldap.pem
    TLSVerifyClient       never
    # =======================================================

    #argsfile /var/run/openldap/slapd.args

    modulepath /usr/lib/ldap
    moduleload back_mdb.la
    moduleload back_monitor.la

    # ----------------------------------------------------------------------
    database mdb
    suffix "dc=edt,dc=org"
    rootdn "cn=Manager,dc=edt,dc=org"
    rootpw secret
    directory /var/lib/ldap
    index objectClass eq,pres
    access to * 
        by self write
        by * read
    # ----------------------------------------------------------------------
    database monitor
    access to *
        by dn.exact="cn=Manager,dc=edt,dc=org" read
        by * none
    ````

    - /etc/ssl/openssl.cnf (añadimos al final del todo)

    ```
    [ practica_req ]

    # Extensions to add to a certificate request
    basicConstraints = CA:FALSE
    keyUsage = nonRepudiation, digitalSignature, keyEncipherment
    subjectAltName = @alt_names

    [ alt_names ]
    DNS.0 = aaron.edt.org
    DNS.1 = mysecureldapserver.org
    DNS.2 = ldap
    IP.1 = 172.18.0.2
    IP.2 = 127.0.0.1
    ```

6. montar imagen tls22:ldaps

Antes que nada, asegurate que tu **startup.sh** tenga permisos de ejecucion.
> docker build -t cristiancondolo21/tls22:ldaps .

---

### Verificacion

7. encender el container en detach

> docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -d cristiancondolo21/tls22:ldaps

Verificamos que este **en detach**.
> docker ps

8. crear el directorio `/etc/ldap/certs` en nuestra local y colocar el certificado de CA

> sudo mkdir /etc/ldap/certs

> sudo cp ca_johnwick_cert.pem /etc/ldap/certs/.

9. modificar nuestro ``ldap.conf`` y ``hosts``
    - ldap.conf

    ````conf
    #
    # LDAP Defaults
    # Certificados-Practica ================================
    TLSCACertificateFile        /etc/ldap/certs/ca_johnwick_cert.pem
    TLSCertificateFile  /etc/ldap/certs/servercert_ldap.pem
    TLSCertificateKeyFile       /etc/ldap/certs/serverkey_ldap.pem
    TLSVerifyClient       never
    # =======================================================

    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE dc=edt,dc=org
    URI ldap://ldap.edt.org

    #SIZELIMIT 12
    #TIMELIMIT 15
    #DEREF  never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT /etc/ldap/certs/ca_johnwick_cert.pem
    ````

    - /etc/hosts

    ````
    172.18.0.2  ldap.edt.org
    ````

10. probamos hacer consultas

    10.1. mostrar **la base**, con **conexion insegura**.
    > ldapsearch -x -H ldap://ldap.edt.org -s base

    <center>
        <img src="Fotos/imagen01.png"/>
    </center>

    10.2. trar **la base**, con **conexion segura**.
    > ldapsearch -x -H ldaps://ldap.edt.org -s base

    <center>
        <img src="Fotos/imagen02.png"/>
    </center>

    10.3. mostrar **el debug**, con **conexion segura** (**leible**).
    > ldapsearch -d1 -x -LLL -H ldaps://ldap.edt.org

    <center>
        <img src="Fotos/imagen03.png"/>
    </center>

    10.4. vuelve una conexion **insegura** a **segura** con **starttls**.
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org

    (si da ERROR al iniciar no pasa nada, funciona igualmente)

    <center>
        <img src="Fotos/imagen04.png"/>
    </center>

    10.5. forzar a usar **starttls**.
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org -s base

    <center>
        <img src="Fotos/imagen05.png"/>
    </center>

    10.6. Verificamos si van por el puerto **inseguro**.
    > ldapsearch -d1 -x -ZZ -H ldap://ldap.edt.org

    (vemos que va por el puerto inseguro 389)

    <center>
        <img src="Fotos/imagen06.png"/>
    </center>

    10.7. ant. lo guarda en un Log y muestra por pantalla
    > ldapsearch -x -ZZ -H ldap://ldap.edt.org -d1 dn 2> log

    <center>
        <img src="Fotos/imagen07.png"/>
    </center>

    10.8. conexion normal especificando host en vez de URI y base manualmente
    > ldapsearch -x -LLL -h 172.18.0.2 -s base -b 'dc=edt,dc=org'

    <center>
        <img src="Fotos/imagen08.png"/>
    </center>

    10.9. intentar iniciar como **starttls** sin especificar URI y como host,
    > ldapsearch -x -LLL -Z -h 172.18.0.2 -s base -b 'dc=edt,dc=org’

    (da error. no funciona por IP, el certificado solo funciona por CN (Common Name))

    <center>
        <img src="Fotos/imagen09.png"/>
    </center>

    > ldapsearch -x -LLL -H ldaps://172.18.0.2 -s base -b 'dc=edt,dc=org'

    <center>
        <img src="Fotos/imagen10.png"/>
    </center>
